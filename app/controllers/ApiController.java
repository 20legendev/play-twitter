package controllers;

import java.util.Date;
import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;

import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import vn.picore.playtwitter.model.Message;

public class ApiController extends Controller {

	@BodyParser.Of(BodyParser.FormUrlEncoded.class)
	public Result postMessage() {

		Map<String, String[]> map = request().body().asFormUrlEncoded();
		Message msg = new Message();
		msg.setMessage(map.get("message")[0]);
		msg.setCreateddate(new Date());
		msg.setCreator("kiennt");

		JsonNode jn = Json.toJson(msg);
		return created(jn);
	}

}
